---
title: Création de ticket
description: 
published: true
date: 2021-03-26T10:28:43.583Z
tags: 
editor: markdown
dateCreated: 2021-03-02T14:03:00.120Z
---

# Création d'un ticket
La création d'un ticket se fait via un formulaire.
Ce formulaire se trouve à l'adresse suivante : 
  - https://novatechnology.with6.glpi-network.cloud/marketplace/formcreator/front/formdisplay.php?id=1
  
  
## Connexion à l'outil
Ce formulmaire utilise une authentification via Office365
Si vous n'êtes pas connecté, il faut cliquer sur le bouton : Azure (Azure AD Nova)

## Remplissage du formulaire
Ce formulaire est assez instinctif.
Il se constitue de plusieurs champ à remplir :
-  La demande
-  Le projet (en fonction de votre type de demande)
-  La description : La déscription doit être la plus précise possible afin de gagner du temps
-  Joindre un fichier : Il est également possible de joindre un fichier afin d'être encore plus précis
-  La priorité : qui se mesure en fonction du temps que l'on dispose pour résoudre le ticket