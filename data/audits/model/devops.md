---
title: Model Audit DevOps
description: 
published: true
date: 2021-03-01T14:17:25.697Z
tags: 
editor: markdown
dateCreated: 2021-03-01T14:09:39.566Z
---

# Modele Audit DevOps

## Trame
* Interview **3 Jours**
* Rapport **4 jours**

## Interview
### Questions
- Organisation des équipes et responsabilités de chacune ?
- Accès nécessaires ?
- Architecture globale Recette, Dev / Prod et interconnexion
- Automatisation de configuration
- Où se trouvent les sources ?
- Organisation des répertoires Git
- Politiques de branching en place
- Y a-t-il une intégration continue / déploiement continue en place ?
- Y a-t-il du chiffrement au repos ?
- Politique de format et de rotation des mots de passe
- Politique de rotation des clés SSH
- Lister les projets applicatifs
- Quelles sont les pratiques de monitoring Alerting ?
- Comment sont collectés les logs ?
- Comment les logs sont mis à disposition ?
- Comment sont gérés les sauvegardes ?
- Comment sont gérés les incidents ?
- Quel est le cycle de vie d’un nouveau projet ?
- Quel est le cycle de vie d’une feature sur un projet déjà en Prod ?
- Outils en jeu ?
- Validation ? 
- Équipes ?
- Où sont les data ?  
- Y a-t-il plusieurs niveaux de criticité ?
- Y a-t-il du chiffrement au repos ?
- Comment les services sont-ils exposés et à qui ?
- Y a-t-il des accès depuis l’extérieur pour du travail à distance ?
- Y a-t-il plusieurs datacenters ?
- Par défaut en IPv4 ou IPv6 ?
- Gestion des DNS ?
- Fédération d’identité ?
- Chiffrement systématique ?
- Politique de rotation des clés et mot passe ?
- Certificats TLS / SSL ?

## Rendu
### Livrable
- Contexte
- Ressenti général
- Terminologie
- Monitoring
- Élasticité
- Mise à disposition et utilisation des ressources
- Gestion de configuration
- Cycle de vie des ressources et applications
- Monitoring
- Collecte de logs
- Processus d’intégration
- Architecture de l’infrastructure
- Moteur applicatif et solution logicielles
- Base de données
- Données
- Préconisations
- Comment améliorer en partant de l’existant ?
- Conclusions
