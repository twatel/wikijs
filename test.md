# VaultWriter

Write secrets into Vault

Requirements arguments : "VAULT_URL, VAULT_SECRET_NAME, VAULT_TOKEN, ACTION, JSON_FILE"

* /!\ Erase all last secrets

The module works with Json File
- Insert json data into the file, then specify the path of the file into JSON_FILE variable
